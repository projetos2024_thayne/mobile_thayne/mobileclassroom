import React, { useState } from "react";
import { View, Button, TextInput, StyleSheet, Text, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const HomeScreen = () => {
  const navigation = useNavigation();
  const [credentials, setCredentials] = useState({ email: "", password: "" });

  const handleLogin = () => {
    // Exemplo de chamada para login
    // api.loginUser(credentials)
    //   .then(response => {
    //     console.log(response.data); // Exibe a resposta da API no console
    //     Alert.alert("Bem vindo",response.data.message)
    navigation.navigate("HomePage", {
      user: "12345680091", //response.data.user.cpf,
      nameUser: "Euller", //response.data.user.name,
    }); // Vá para a próxima tela após Login
    Alert.alert("Bem vindo");
    //   })
    //   .catch(error => {
    //     Alert.alert("Erro",error.response.data.error);
    //   });
  };

  const handleSignupRedirect = () => {
    Alert.alert("OBS", "Achou que eu iria te entregar tudo pronto?");
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Email"
        placeholderTextColor="#fff"
        onChangeText={(text) => setCredentials({ ...credentials, email: text })}
        value={credentials.email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setCredentials({ ...credentials, password: text })
        }
        value={credentials.password}
        secureTextEntry
      />
      <Button 
        title="Login" 
        onPress={handleLogin} 
        color="green" 
      />

      <Button
        title="Cadastre-se"
        onPress={handleSignupRedirect}
        color="green"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#084d6e",
  },
  input: {
    width: "80%",
    marginBottom: 10,
    padding: 15,
    color: "#fff",
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 10,
  },
});

export default HomeScreen;
