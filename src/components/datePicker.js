import React, {useState} from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = ({type, buttonTitle, dateKey, setSchedule}) => {
 const [datePickerVisible,setDatePickerVisible] = useState (false)

 const showDatePicker = () => {
    setDatePickerVisible(true);
 }

 const hideDatePicker = () => {
    setDatePickerVisible(false);
 };

 const handleConfirm = (date) => {
    if(type === "time"){
        //Lógica para extrair hora e minuto
        const hour = date.getHours();
        const minute = date.getMinutes();

        //Lógica para montar hora e minuto no formato desejado
        const formattedTime = `${hour}:${minute}`;

        //Atualiza o estado da reserva com HH:mm formatada
        setSchedule((prevState)=>({
            ...prevState,
            [dateKey]:formattedTime,
        }));
    }
    else {
        const formattedDate = date.toISOString().split('T')[0];
        //Atualizar Schedule
        setSchedule((prevState)=>({
            ...prevState,
            [dateKey]:formattedDate,
        }));
    }
    hideDatePicker();
 };

 return(
    <View>
        <Button title={buttonTitle} onPress={showDatePicker} color="pink"/>
        <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        //Estilo opicional
        // pickerComponentStyleIOS={{backgroundColor:'#fff'}}
        // textColor="#000"
        />
        </View>
 );

}; //Fim da const DateTimePicker
export default DateTimePicker;